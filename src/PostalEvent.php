<?php

namespace Civi\Postal;

use Symfony\Component\EventDispatcher\Event;

/**
 * Represents an event received through a Postal webhook.
 */
class PostalEvent extends Event {

  const NAME = 'civi.postal.webhook';

  /**
   * The event name.
   *
   * @var string
   */
  protected $name;

  /**
   * The event payload.
   *
   * @var array
   */
  protected $payload;

  /**
   * PostalEvent constructor.
   *
   * @param string $name
   *   The event name.
   * @param array $payload
   *   The event payload.
   */
  public function __construct($name, array $payload) {
    $this->name = $name;
    $this->payload = $payload;
  }

  /**
   * Gets the event name.
   *
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Gets the event payload.
   *
   * @return array
   */
  public function getPayload() {
    return $this->payload;
  }

}